package com.tlz.guide

/**
 *
 * Created by Tomlezen.
 * Date: 2017/7/28.
 * Time: 17:40.
 */
object Location {

    const val BOTTOM = 80
    const val CENTER = 17
    const val CENTER_HORIZONTAL = 1
    const val CENTER_VERTICAL = 16
    const val END = 8388613
    const val START = 8388611
    const val TOP = 48

}