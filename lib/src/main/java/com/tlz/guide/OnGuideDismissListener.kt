package com.tlz.guide

/**
 *
 * Created by Tomlezen.
 * Date: 2017/7/29.
 * Time: 15:29.
 */
interface OnGuideDismissListener {

    fun onDismiss()

}